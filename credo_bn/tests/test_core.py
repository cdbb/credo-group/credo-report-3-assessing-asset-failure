"""
Tests for the credo_bn module
"""

import unittest
import credo_bn.core


class TestBasicModel(unittest.TestCase):
    def test_inbuilt(self):
        model, _ = credo_bn.core.build_flood_model()
        return model.check_model()


class TestFromConfig(unittest.TestCase):
    def test_fromfile(self):
        model = credo_bn.core.Bayesnet()
        model.build_model(
            model_path="../../example_files/model_config/single_stochastic_node.json"
        )
        return model.network.check_model()


class TestBinning(unittest.TestCase):
    def test_min(self):
        return credo_bn.core.assign_bin(-1, [0, 1, 2]) == 0

    def test_max(self):
        return credo_bn.core.assign_bin(5, [0, 1, 2]) == 3

    def test_mid(self):
        return credo_bn.core.assign_bin(0.5, [0, 1, 2]) == 1


if __name__ == "__main__":
    unittest.main()
