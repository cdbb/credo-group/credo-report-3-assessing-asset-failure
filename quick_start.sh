#!/bin/bash

echo "What do you want to run?"
echo "1 - build container"
echo "2 - launch container interactively"
echo "3 - run model locally.py"
read -r ans

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

if [[ $ans == "1" ]]; then
    docker build -t flood-impact-model .
elif [[ $ans == "2" ]]; then
    docker run --rm -v $SCRIPT_DIR/data:/data -it flood-impact-model bash
elif [[ $ans == "3" ]]; then
    docker run --rm -v $SCRIPT_DIR/data:/data flood-impact-model
else
    echo "Answer 1, 2, or 3."
    exit 1
fi
