# bayesnet/Dockerfile
FROM python:3.9-slim-bullseye
WORKDIR /model_src
RUN pip install --no-cache-dir poetry

COPY credo_bn/ credo_bn
COPY poetry.lock pyproject.toml ./
RUN poetry install --no-dev \
    && rm -rf /root/.cache/pypoetry/cache /root/.cache/pypoetry/artifacts
COPY scripts/ ./scripts
CMD ["poetry", "run", "python", "scripts/ClassifyAssets.py"]
